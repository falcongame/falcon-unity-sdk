using Falcon.FalconCore.Scripts.FalconABTesting.Scripts.Model;

namespace Falcon.FalconGoogleUMP
{
    public class FalconUMPConfig : FalconConfig
    {
        public int f_ump_consent = 1;
        public bool f_reset_cmp = false;
    }
}
