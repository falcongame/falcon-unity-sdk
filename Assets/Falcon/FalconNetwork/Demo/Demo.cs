using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Falcon;

public class Demo : MonoBehaviour
{
    // Start is called before the first frame update
    void Awake()
    {
        FSocketIOChannel channel = new FSocketIOChannel("http://localhost:11111/demo/");
        FSession session = new FSession(channel);
        session.AddListener(new NetListener());
        session.Start();
    }
}
