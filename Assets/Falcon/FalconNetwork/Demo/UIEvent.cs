using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIEvent : MonoBehaviour
{
    public Button sendButton;
    public InputField textField;
    // Start is called before the first frame update
    public void ButtonClick()
    {
        Debug.Log(textField.text);
        new CSChat("Hi you!").Send();
    }
}
