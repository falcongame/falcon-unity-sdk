using Falcon;

    public class CSChat : CSMessage
    {
        public string text;

        public CSChat() { }

        public CSChat(string text)
        {
            this.text = text;
        }

        public override string GetEvent()
        {
            return "c_chat";
        }
    }

