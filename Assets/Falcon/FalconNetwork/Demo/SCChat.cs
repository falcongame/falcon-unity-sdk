using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Falcon;

    public class SCChat: SCMessage
    {
        public string text;
        public override string GetEvent()
        {
            return "s_chat";
        }

        public override void OnData()
        {
            Debug.Log("___________" + text);
        }
    }
