#if !BESTHTTP_DISABLE_ALTERNATE_SSL && (!UNITY_WEBGL || UNITY_EDITOR)
#pragma warning disable
using System;

using BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Tsp;
using BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.X509;
using BestHTTP.SecureProtocol.Org.BouncyCastle.Math;

namespace BestHTTP.SecureProtocol.Org.BouncyCastle.Tsp
{
	public class TimeStampTokenInfo
	{
		private TstInfo		tstInfo;
		private DateTime	genTime;

		public TimeStampTokenInfo(
			TstInfo tstInfo)
		{
			this.tstInfo = tstInfo;

			try
			{
				this.genTime = tstInfo.GenTime.ToDateTime();
			}
			catch (Exception e)
			{
				throw new TspException("unable to parse genTime field: " + e.Message);
			}
		}

		public bool IsOrdered => tstInfo.Ordering.IsTrue;

		public Accuracy Accuracy => tstInfo.Accuracy;

		public DateTime GenTime => genTime;

		public GenTimeAccuracy GenTimeAccuracy =>
			this.Accuracy == null
				?	null
				:	new GenTimeAccuracy(this.Accuracy);

		public string Policy => tstInfo.Policy.Id;

		public BigInteger SerialNumber => tstInfo.SerialNumber.Value;

		public GeneralName Tsa => tstInfo.Tsa;

		/**
		 * @return the nonce value, null if there isn't one.
		 */
		public BigInteger Nonce =>
			tstInfo.Nonce == null
				?	null
				:	tstInfo.Nonce.Value;

		public AlgorithmIdentifier HashAlgorithm => tstInfo.MessageImprint.HashAlgorithm;

		public string MessageImprintAlgOid => tstInfo.MessageImprint.HashAlgorithm.Algorithm.Id;

		public byte[] GetMessageImprintDigest()
		{
			return tstInfo.MessageImprint.GetHashedMessage();
		}

		public byte[] GetEncoded()
		{
			return tstInfo.GetEncoded();
		}

		public TstInfo TstInfo => tstInfo;
	}
}
#pragma warning restore
#endif
