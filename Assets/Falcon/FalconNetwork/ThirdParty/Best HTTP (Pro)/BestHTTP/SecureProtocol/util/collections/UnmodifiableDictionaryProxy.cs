#if !BESTHTTP_DISABLE_ALTERNATE_SSL && (!UNITY_WEBGL || UNITY_EDITOR)
#pragma warning disable
using System;
using System.Collections;

namespace BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections
{
	public class UnmodifiableDictionaryProxy
		: UnmodifiableDictionary
	{
		private readonly IDictionary d;

		public UnmodifiableDictionaryProxy(IDictionary d)
		{
			this.d = d;
		}

		public override bool Contains(object k)
		{
			return d.Contains(k);
		}

		public override void CopyTo(Array array, int index)
		{
			d.CopyTo(array, index);
		}

		public override int Count => d.Count;

		public override IDictionaryEnumerator GetEnumerator()
		{
			return d.GetEnumerator();
		}

		public override bool IsFixedSize => d.IsFixedSize;

		public override bool IsSynchronized => d.IsSynchronized;

		public override object SyncRoot => d.SyncRoot;

		public override ICollection Keys => d.Keys;

		public override ICollection Values => d.Values;

		protected override object GetValue(object k)
		{
			return d[k];
		}
	}
}
#pragma warning restore
#endif
