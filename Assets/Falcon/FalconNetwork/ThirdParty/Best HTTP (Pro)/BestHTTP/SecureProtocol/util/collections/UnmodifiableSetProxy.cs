#if !BESTHTTP_DISABLE_ALTERNATE_SSL && (!UNITY_WEBGL || UNITY_EDITOR)
#pragma warning disable
using System;
using System.Collections;

namespace BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities.Collections
{
	public class UnmodifiableSetProxy
		: UnmodifiableSet
	{
		private readonly ISet s;

		public UnmodifiableSetProxy (ISet s)
		{
			this.s = s;
		}

		public override bool Contains(object o)
		{
			return s.Contains(o);
		}

		public override void CopyTo(Array array, int index)
		{
			s.CopyTo(array, index);
		}

		public override int Count => s.Count;

		public override IEnumerator GetEnumerator()
		{
			return s.GetEnumerator();
		}

		public override bool IsEmpty => s.IsEmpty;

		public override bool IsFixedSize => s.IsFixedSize;

		public override bool IsSynchronized => s.IsSynchronized;

		public override object SyncRoot => s.SyncRoot;
	}
}
#pragma warning restore
#endif
