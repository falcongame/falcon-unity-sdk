#if !BESTHTTP_DISABLE_ALTERNATE_SSL && (!UNITY_WEBGL || UNITY_EDITOR)
#pragma warning disable
using System;

using BestHTTP.SecureProtocol.Org.BouncyCastle.Utilities;

namespace BestHTTP.SecureProtocol.Org.BouncyCastle.Crypto.Tls
{
    public class SecurityParameters
    {
        internal int entity = -1;
        internal int cipherSuite = -1;
        internal byte compressionAlgorithm = CompressionMethod.cls_null;
        internal int prfAlgorithm = -1;
        internal int verifyDataLength = -1;
        internal byte[] masterSecret = null;
        internal byte[] clientRandom = null;
        internal byte[] serverRandom = null;
        internal byte[] sessionHash = null;
        internal byte[] pskIdentity = null;
        internal byte[] srpIdentity = null;

        // TODO Keep these internal, since it's maybe not the ideal place for them
        internal short maxFragmentLength = -1;
        internal bool truncatedHMac = false;
        internal bool encryptThenMac = false;
        internal bool extendedMasterSecret = false;

        internal virtual void Clear()
        {
            if (this.masterSecret != null)
            {
                Arrays.Fill(this.masterSecret, (byte)0);
                this.masterSecret = null;
            }
        }

        /**
         * @return {@link ConnectionEnd}
         */
        public virtual int Entity => entity;

        /**
         * @return {@link CipherSuite}
         */
        public virtual int CipherSuite => cipherSuite;

        /**
         * @return {@link CompressionMethod}
         */
        public virtual byte CompressionAlgorithm => compressionAlgorithm;

        /**
         * @return {@link PRFAlgorithm}
         */
        public virtual int PrfAlgorithm => prfAlgorithm;

        public virtual int VerifyDataLength => verifyDataLength;

        public virtual byte[] MasterSecret => masterSecret;

        public virtual byte[] ClientRandom => clientRandom;

        public virtual byte[] ServerRandom => serverRandom;

        public virtual byte[] SessionHash => sessionHash;

        public virtual byte[] PskIdentity => pskIdentity;

        public virtual byte[] SrpIdentity => srpIdentity;

        public virtual bool IsExtendedMasterSecret => extendedMasterSecret;
    }
}
#pragma warning restore
#endif
