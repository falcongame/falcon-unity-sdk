﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Falcon
{
    public abstract class FChannel
    {
        private FSession session;

        public abstract void Start();

        public abstract void Reconnect();

        public abstract void Disconnect();

        protected abstract void SendMessage(FMessage message);

        public void Send(FMessage message)
        {
            LogUtil.debug(message);
            SendMessage(message);
        }

        public void OnConnected()
        {
            session.OnChannelConnected();
        }

        public void OnDisconnected()
        {
            session.OnChannelDisconnected();
        }

        public FSession GetSession()
        {
            return session;
        }

        public void SetSession(FSession session)
        {
            this.session = session;
        }

        public void OnMessage(FMessage message)
        {
            if (message is SCInitSession) {
                SCInitSession initSession = (SCInitSession)message;
                if (initSession.GetState() == SCInitSession.STATE_NEW_SESSION)
                {
                    session.NewSession();
                }
                else
                {
                    session.ContinueCurrentSession();
                }
            } else {
                session.OnMessage(message);
            }
        }
    }
}

