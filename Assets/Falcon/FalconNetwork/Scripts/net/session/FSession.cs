﻿using System;
using System.Collections.Generic;
using System.Threading;
using Falcon.FalconCore.Scripts.Repositories;
using UnityEngine;

namespace Falcon
{
    public class FSession : ISession
    {
        private FChannel channel;
        private List<ISessionListener> listeners = new List<ISessionListener>();
        private string sessionId;
        private bool isStarted = false;
        private bool forceDisconnected = false;

        private long maxCSSequence;
        private long maxSCSequence;

        private int firstSession = 0;

        private Queue<FMessage> importantQueue = new Queue<FMessage>();

        public FSession(FChannel channel)
        {
            this.channel = channel;
            this.channel.SetSession(this);
            this.sessionId = System.Guid.NewGuid().ToString();
        }

        public List<ISessionListener> getListeners()
        {
            return listeners;
        }

        public string GetSessionId()
        {
            return sessionId;
        }

        public bool IsActive()
        {
            return isStarted;
        }

        public void OnChannelConnected()
        {
            Debug.Log("Channel connected!");
            channel.Send(new CSInitSession(sessionId));
        }


        public void OnChannelDisconnected()
        {
            Debug.Log("Channel disconnected!");
            if (!this.forceDisconnected)
            {
                channel.Reconnect();
            }
        }


        public void OnMessage(FMessage message)
        {
            LogUtil.debug(message);
            if (message is SCCloseSession)
            {
                this.forceDisconnected = true;
                this.channel.Disconnect();
            }else if (maxSCSequence < message.scSequence){
                maxSCSequence = message.scSequence;
                long csSequence = message.csSequence;
                while(importantQueue.Count > 0)
                {
                    FMessage m = importantQueue.Peek();
                    if (m.csSequence <= csSequence)
                        importantQueue.Dequeue();
                    else
                        break;
                }

                if (message.response == 1)
                {
                    this.Send(new CSSCResponse(message.messageId, true, ""));
                }

                SCMessage scMessage = (SCMessage)message;
                scMessage.OnData();

                if (message.response == 2)
                {
                    this.Send(new CSSCResponse(message.messageId, true, ""));
                }

            }
        }

        public void Send(FMessage message)
        {
            if (this.isStarted)
            {
                if (!(message is FPing))
                {
                    message.csSequence = ++maxCSSequence;
                    message.scSequence = maxSCSequence;
                }
                channel.Send(message);
            }
        }

        public void Send(FMessage message, bool important)
        {
            if (this.isStarted && !this.forceDisconnected)
            {
                message.timeClient = FTime.CurrentTimeMillis();
                if (!(message is FPing)) {
                    message.csSequence = (++maxCSSequence);
                    message.scSequence = (maxSCSequence);
                }
                if (important)
                {
                    importantQueue.Enqueue(message);
                }
                channel.Send(message);
            }
        }

        public FFuture Send(FMessage message, FFuture future, int timeOut)
        {
            if (future == FFuture.RECEIVED)
            {
                message.response = 1;
            }
            else if (future == FFuture.PROCESSED)
            {
                message.response = 2;
            }
            String messageId = Guid.NewGuid().ToString();
            message.messageId = messageId;
            Send(message);
            FFuture fFuture = new FFuture(messageId, timeOut);
            FFutureManager.Instance.Put(messageId, fFuture);
            return fFuture;
        }

        public FFuture Send(FMessage message, FFuture future)
        {
            return Send(message, future, 5);
        }

        public void SetTimeout(int sec)
        {

        }

        public void NewSession()
        {
            Debug.Log("new session");
            isStarted = true;
            maxCSSequence = 0;
            maxSCSequence = 0;
            importantQueue.Clear();
            forceDisconnected = false;

            foreach (ISessionListener listener in listeners)
            {
                if (firstSession == 0)
                    listener.onFirstSession(this);
                listener.OnNewSession(this);
            }
            firstSession++;
        }


        public void Start()
        {
            channel.Start();
            new Thread(() =>
            {
                while (!forceDisconnected)
                {
                    Send(new FPing());
                    Thread.Sleep(5000);

                }
            }).Start();
        }


        public void ContinueCurrentSession()
        {
            while(importantQueue.Count > 0)
            {
                FMessage m = importantQueue.Dequeue();
                channel.Send(m);
            }
            foreach (ISessionListener listener in listeners)
            {
                listener.OnContinueCurrentSession(this);
            }
        }

        public void AddListener(ISessionListener listener)
        {
            listeners.Add(listener);
        }

        public void RemoveListener(ISessionListener listener)
        {
            listeners.Remove(listener);
        }

        public void RemoveAllListeners()
        {
            listeners.Clear();
        }
    }
}

