using System;
using System.Collections;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using UnityEngine;
using Falcon;
using Falcon.FalconCore.Scripts.Services.GameObjs;
using Newtonsoft.Json.Linq;

public class UDPNetworkManager
{
    private UDPNetworkManager()
    {
    }

    private static UDPNetworkManager _instance = null;

    public static UDPNetworkManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new UDPNetworkManager();
                _instance.Init();
            }

            return _instance;
        }
    }

    private const string Separator = ":";
    private IPAddress _serverIp;
    private IPEndPoint _hostEndPoint;

    private IPEndPoint _localIpPort;
    private UdpClient _client;
    private Thread _thread;

    private void Init()
    {
        _serverIp = IPAddress.Parse("192.168.1.8");
        _hostEndPoint = new IPEndPoint(_serverIp, 16110);

        _client = new UdpClient();
        _client.Connect(_hostEndPoint);
        _client.Client.Blocking = false;
        _client.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, 2000);
        _localIpPort = new IPEndPoint(_serverIp, ((IPEndPoint) _client.Client.LocalEndPoint).Port);
        // _thread = new Thread(ReceiveMessage) { IsBackground = true };
        // _thread.Start();
        FGameObj.Instance.StartCoroutine(ReceiveMessage());
    }

    private IEnumerator ReceiveMessage()
    {
        while (true)
        {
            if (_client.Available > 0)
            {
                try
                {
                    var content = _client.Receive(ref _localIpPort);
                    var data = Encoding.ASCII.GetString(content);
                    var idx = data.IndexOf(Separator, StringComparison.Ordinal);
                    var eventName = data.Substring(0, idx);
                    var json = data.Substring(idx + 1);
                    var udpEvent = (SCMessage)JObject.Parse(json).ToObject(FClientManager.Instance.getEventType(eventName));
                    LogUtil.debug(udpEvent);
                    udpEvent.OnData();
                }
                catch (Exception ex)
                {
                }
            }
            else
            {
                yield return new WaitForSeconds(0.01f);
            }
        }
    }

    public void Send(CSMessage message)
    {
        StringBuilder builder = new StringBuilder();
        builder.Append(message.GetEvent());
        builder.Append(Separator);
        builder.Append(SocketClient.Instance.GetSessionId());
        builder.Append(Separator);
        builder.Append(JsonUtility.ToJson(message));

        var data = Encoding.ASCII.GetBytes(builder.ToString());

        _client.Send(data, data.Length);
    }
}