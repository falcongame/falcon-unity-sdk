﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Falcon
{
    public class FClientManager
    {
        private FClientManager()
        {

        }
        private static FClientManager instance = null;
        public static FClientManager Instance
        {
            get {
                if (instance == null)
                {
                    instance = new FClientManager();
                    instance.Init();
                }
                return instance; 
            }

        }

        private IEnumerable<Type> scClasses;
        private Dictionary<string, Type> evt2Type = new Dictionary<string, Type>();

        private void Init()
        {
            scClasses = typeof(SCMessage).Assembly.GetTypes().Where(t => t.IsSubclassOf(typeof(SCMessage)));
            foreach (var type in scClasses)
            {
                if (type.IsSubclassOf(typeof(SCMessage)))
                {
                    SCMessage message = (SCMessage) Activator.CreateInstance(type);
                    evt2Type[message.GetEvent()] = message.GetType();
                }
            }
        }

        public IEnumerable<Type> GetSCClasses()
        {
            return scClasses;
        }
        
        public Type getEventType(string evt)
        {
            return evt2Type[evt];
        }
    }
}


