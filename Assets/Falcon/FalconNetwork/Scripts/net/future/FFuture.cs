using System;
using Falcon.FalconCore.Scripts.Repositories;

namespace Falcon
{
    public class FFuture {

        public static FFuture RECEIVED = new FFuture();
        public static FFuture PROCESSED = new FFuture();

        public Action<CSSCResponse> onResponse;
        private int timeOut;
        private long timeSend;
        private string messageId;

        public FFuture() {
        }

        public FFuture(string messageId, int timeOut) {
            this.messageId = messageId;
            this.timeOut = timeOut + (int) (FTime.CurrentTimeMillis() / 1000);
            this.timeSend = FTime.CurrentTimeMillis();
        }

        public int GetTimeOut() {
            return timeOut;
        }

        public long GetTimeSend() {
            return timeSend;
        }

        public string GetMessageId() {
            return messageId;
        }
        
        public void AddActionOnResponse(Action<CSSCResponse> onResponse) {
            this.onResponse = onResponse;
        }
        
        public void OnResponse(CSSCResponse response){            
            onResponse(response);
        }

        public void OnTimeout()
        {

        }
        
    }
}