using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using Falcon.FalconCore.Scripts.Repositories;
using Falcon.FalconCore.Scripts.Services.GameObjs;
using UnityEngine;

namespace Falcon
{
    public class FFutureManager {
        
        private static readonly FFutureManager instance = new FFutureManager();
        
        private FFutureManager() {}

        public static FFutureManager Instance => instance;

        private Dictionary<string, FFuture> messageId2Future = new Dictionary<string, FFuture>(); 
        private Dictionary<int, HashSet<FFuture>> timeout2Futures = new Dictionary<int, HashSet<FFuture>>();
        private Thread t;

        public void Start()
        {
            FGameObj.Instance.StartCoroutine(Run());
        }


        private IEnumerator Run()
        {
            while (true)
            {
                Dictionary<int, HashSet<FFuture>> entries = new Dictionary<int, HashSet<FFuture>>(timeout2Futures);
                foreach (KeyValuePair<int, HashSet<FFuture>> entry in entries)
                {
                    if(entry.Key < FTime.CurrentTimeMillis() / 1000) {
                        foreach(FFuture future in entry.Value) {
                            future.OnTimeout();
                        }

                        try
                        {
                            timeout2Futures.Remove(entry.Key);
                        }
                        catch (Exception)
                        {
                            //ignore
                        }
                    }
                    yield return new WaitForSeconds(2.0f);
                }
                
                yield return new WaitForSeconds(0.01f);
            }
        }
        public void Put(string messageId, FFuture future) {
            messageId2Future.Add(messageId, future);
            int sec = future.GetTimeOut();
            if (!timeout2Futures.ContainsKey(sec)) {
                timeout2Futures.Add(sec, new HashSet<FFuture>());
            }
            timeout2Futures[sec].Add(future);
        }

        public void OnResponse(CSSCResponse message)
        {
            if (messageId2Future.ContainsKey(message.messageId))
            {
                FFuture future = messageId2Future[message.messageId];
                future.OnResponse(message);
                try{
                    messageId2Future.Remove(message.messageId);
                }
                catch (Exception)
                {
                    //ignore
                }
                timeout2Futures[future.GetTimeOut()].Remove(future);
            }
        }

    }
}