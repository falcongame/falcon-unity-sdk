﻿using BestHTTP.SocketIO;
using System;
using BestHTTP.SocketIO.JsonEncoders;
using Newtonsoft.Json.Linq;

namespace Falcon
{
    public class FSocketIOChannel : FChannel
    {
        private SocketManager socket;
        private string uri;

        public FSocketIOChannel(string uri)
        {
            this.uri = uri;
        }

        private Func<object, object> OnStartListening;

        public override void Reconnect()
        {

        }

        public override void Start()
        {
            SocketOptions options = new SocketOptions
            {
                AutoConnect = false,
                ConnectWith = BestHTTP.SocketIO.Transports.TransportTypes.WebSocket,
            };
            socket = new SocketManager(new Uri(uri), options);
            socket.Encoder = new LitJsonEncoder();

            socket.Socket.On(SocketIOEventTypes.Connect, (Socket socket, Packet packet, object[] args) =>
            {
                OnConnected();
            });
            socket.Socket.On(SocketIOEventTypes.Disconnect, (Socket socket, Packet packet, object[] args) =>
            {
                OnDisconnected();
            });
            socket.Socket.On(SocketIOEventTypes.Error, (Socket socket, Packet packet, object[] args) =>
            {
                OnDisconnected();
            });
            socket.Socket.On("reconnect", (Socket socket, Packet packet, object[] args) =>
            {

            });
            socket.Socket.On("reconnecting", (Socket socket, Packet packet, object[] args) =>
            {

            });
            socket.Socket.On("reconnect_attempt", (Socket socket, Packet packet, object[] args) =>
            {

            });
            socket.Socket.On("reconnect_failed", (Socket socket, Packet packet, object[] args) =>
            {

            });


            foreach (var type in FClientManager.Instance.GetSCClasses())
            {
                if (type.IsSubclassOf(typeof(SCMessage)))
                {
                    SCMessage message = (SCMessage)Activator.CreateInstance(type);
                    socket.Socket.On(message.GetEvent(), (socket, packet, args) =>
                    {
                        JArray array = JArray.Parse(packet.Payload);
                        var json = array[1].ToString();

                        message = (SCMessage)JObject.Parse(json).ToObject(type);
                        this.OnMessage(message);
                    });
                }
            }

            socket.Open();
        }

        private void OnServerConnect(Socket socket, Packet packet, object[] args)
        {

        }

        protected override void SendMessage(FMessage message)
        {
            socket.Socket.Emit(message.GetEvent(), message);
        }

        public override void Disconnect()
        {
            socket.Close();
        }
    }
}

