﻿using System;
using Falcon.FalconCore.Scripts.Repositories;

namespace Falcon
{
    [Serializable]
    public abstract class FMessage
    {
        public string messageId;
        public long csSequence;
        public long scSequence;
        public long timeServer;

        public long timeClient = FTime.CurrentTimeMillis();

        public int numberClone = -1;
        public bool needResponse;
        public int response;//=0 không cần response, =1 response trước khi thực thi, =2 response sau khi thực thi
        public abstract string GetEvent();

    }
}

