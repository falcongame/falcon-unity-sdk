using System;
namespace Falcon
{
    [Serializable]
    public class CSSCResponse : SCMessage
    {
        public bool success;
        public string message;

        public override string GetEvent()
        {
        return "cs_sc_response";
        }

        public override void OnData()
        {
            FFutureManager.Instance.OnResponse(this);
            //throw new NotImplementedException();
        }

        public CSSCResponse() { }

        public CSSCResponse(string messageId, bool success, string message)
        {
            this.messageId = messageId;
            this.success = success;
            this.message = message;
        }
    }
}

