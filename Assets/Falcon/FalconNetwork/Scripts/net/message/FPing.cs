﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Falcon
{
    [Serializable]
    public class FPing : CSMessage
    {
        public override string GetEvent()
        {
            return "f_ping";
        }
    }
}

