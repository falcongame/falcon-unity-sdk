﻿using System;
namespace Falcon
{
    [Serializable]
    public abstract class CSMessage : FMessage
    {
        public enum TransportType
        {
            TCP,
            UDP
        }
        public void Send()
        {
            SocketClient.Instance.Send(this);
            
        }

        public void Send(bool important)
        {
            SocketClient.Instance.Send(this, important);
        }

        public FFuture Send(FFuture future, int timeOut)
        {
            return SocketClient.Instance.Send(this, future, timeOut);
        }

        public FFuture Send(FFuture future)
        {
            return SocketClient.Instance.Send(this, future);
        }

        public void Send(TransportType transportType)
        {
            if (transportType == TransportType.TCP)
                Send();
            else if (transportType == TransportType.UDP)
            {
                UDPNetworkManager.Instance.Send(this);
            }
        }
    }
}

