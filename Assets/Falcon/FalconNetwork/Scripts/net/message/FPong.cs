﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Falcon
{
    [Serializable]
    public class FPong : SCMessage
    {
        public override string GetEvent()
        {
            return "f_pong";
        }

        public override void OnData()
        {

        }
    }
}

