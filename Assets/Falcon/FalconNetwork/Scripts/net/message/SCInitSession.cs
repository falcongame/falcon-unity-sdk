﻿using System;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Falcon
{
    [Serializable]
    public class SCInitSession : SCMessage
    {
        public const int STATE_NEW_SESSION = 0;
        public const int STATE_OLD_SESSION = 1;
        public int state;

        public override string GetEvent()
        {
            return "f_sc_init_session"; 
        }

        public override void OnData()
        {
            // Thread thread = new Thread(this.SendPing);
            // thread.IsBackground = true;
            // thread.Start();
        }

        public int GetState()
        {
            return state;
        }

        private void SendPing()
        {
            // while (true)
            // {
            //     SocketClient.Instance.Send(new FPing());
            //     Thread.Sleep(5000);
            // }
        }
    }
}

