﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Falcon
{
    [Serializable]
    public class CSInitSession : CSMessage
    {
        public string sessionId;

        public CSInitSession(string sessionId)
        {
            this.sessionId = sessionId;
        }

        public override string GetEvent()
        {
            return "f_cs_init_session";
        }
    }
}

