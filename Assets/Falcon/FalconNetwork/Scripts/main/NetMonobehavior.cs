﻿using System.Collections;
using System.Collections.Generic;
using Falcon;
using UnityEngine;

public class NetMonobehavior : MonoBehaviour
{
    // Start is called before the first frame update
    void Awake()
    {
        FSocketIOChannel channel = new FSocketIOChannel("http://localhost:11112/test/");
        FSession session = new FSession(channel);
        session.AddListener(new NetListener());
        session.Start();
    }
}
