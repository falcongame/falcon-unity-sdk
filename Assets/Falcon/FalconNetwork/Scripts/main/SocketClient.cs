using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Falcon
{
    public sealed class SocketClient
    {
        private static readonly SocketClient instance = new SocketClient();

        static SocketClient() {}
        private SocketClient() {}

        private FSession activeSession;

        public static SocketClient Instance => instance;

        public void SetActiveSession(FSession session)
        {
            this.activeSession = session;
        }

        public void Send(CSMessage cSMessage, bool important)
        {
           this.activeSession.Send(cSMessage, important);
        }

        public FFuture Send(CSMessage cSMessage, FFuture fFuture, int timeOut = 5)
        {
            return this.activeSession.Send(cSMessage, fFuture, timeOut);
        }

        public void Send(CSMessage cSMessage)
        {
             this.activeSession.Send(cSMessage, false);
        }

        public FFuture Send(CSMessage cSMessage, FFuture future)
        {
            return this.activeSession.Send(cSMessage, future);
        }

        public string GetSessionId()
        {
            return this.activeSession.GetSessionId();
        }
    }
}
