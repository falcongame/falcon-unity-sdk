﻿using System;
using System.Threading;
using UnityEngine;

namespace Falcon
{
    public class NetListener : ISessionListener
    {
        public void OnChannelConnected(FSession session)
        {

        }

        public void OnChannelDisconnected(FSession session)
        {

        }

        public void OnContinueCurrentSession(FSession session)
        {

        }

        public void onFirstSession(FSession fSession)
        {
            SocketClient.Instance.SetActiveSession(fSession);
            FFutureManager.Instance.Start();
            // new CSChat("Hello").Send(CSMessage.TransportType.UDP);
            new CSChat("Hello").Send(FFuture.PROCESSED).AddActionOnResponse(response => Debug.Log("Ket qua"));
        }

        public void OnTimeout(FSession fSession)
        {

        }


        public void OnNewSession(FSession fSession)
        {

        }
    }
}
