﻿using System.IO;
using UnityEditor;

public class AppsFlyerSettingsInspector : Editor
{
    private static AppsFlyerSettings appsFlyerSettings;
    public static string PATH_ASSET = "Assets/AppsFlyer/Resources/AppsFlyerSettings.asset";
    public static AppsFlyerSettings AppsFlyerSettings
    {
        get
        {
            if (appsFlyerSettings == null)
            {
                appsFlyerSettings = AssetDatabase.LoadAssetAtPath<AppsFlyerSettings>(PATH_ASSET);
                if (appsFlyerSettings == null)
                {
                    AppsFlyerSettings asset = CreateInstance<AppsFlyerSettings>();
                    Directory.CreateDirectory("Assets/AppsFlyer/Resources");
                    AssetDatabase.CreateAsset(asset, PATH_ASSET);
                    appsFlyerSettings = asset;
                }
            }
            return appsFlyerSettings;
        }
    }
}