﻿using System.IO;
using UnityEditor;
using UnityEngine;

public class AppsFlyerMenu : Editor
{
    [MenuItem("Falcon/Falcon Analytic/AppsFlyer settings", false, 3)]
    public static void mediationSettings()
    {
        var appsFlyerSettings = AppsFlyerSettingsInspector.AppsFlyerSettings;
        Selection.activeObject = appsFlyerSettings;
    }
}