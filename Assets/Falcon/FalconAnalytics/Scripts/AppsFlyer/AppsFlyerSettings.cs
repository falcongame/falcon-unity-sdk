﻿using UnityEngine;

public class AppsFlyerSettings : ScriptableObject
{
    [Header("AppsFlyer DevKey")]
    [Tooltip("AppsFlyer's Dev Key, which is accessible from the AppsFlyer dashboard.")]
    public string devKey = string.Empty;
    [Tooltip("Your iTunes Application ID. (If your app is not for iOS the leave field empty)")]
    public string appID = string.Empty;
    [Tooltip("Set this to true if your app is using AppsFlyer for deep linking.")]
    public bool getConversionData = false;
}