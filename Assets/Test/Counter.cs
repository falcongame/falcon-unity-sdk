using UnityEngine;
using UnityEngine.UI;

namespace Test
{
    public class Counter : MonoBehaviour
    {
        [SerializeField] private Text text;

        private ulong counter;
        // Start is called before the first frame update
        void Start()
        {
            counter = 1;
        }

        // Update is called once per frame
        void Update()
        {
            counter++;
            text.text = counter + "";
        }
    }
}
