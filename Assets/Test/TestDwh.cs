using Falcon.FalconGoogleUMP;
// using Falcon.FalconMediation.Core;
using UnityEngine;

namespace Test
{
    public class TestDwh : MonoBehaviour
    {
        private void Start()
        {
            FalconUMP.ShowConsentForm((consen) =>
            {
                // FalconMediation.InitIronSource(consen);
            }, () =>
            {
                // FalconMediation.InitGMA();
            }, null);
        }

        public void ShowBanner()
        {
            // FalconMediation.ShowBanner();
        }

        public void HideBanner()
        {
            // FalconMediation.HideBanner();
        }

        public void ShowIntertitial()
        {
            // FalconMediation.ShowInterstitial();
        }

        public void ShowRewarded()
        {
            // FalconMediation.ShowRewardedVideo(() => { Debug.Log("success"); }, () => { Debug.Log("failure"); }, "test");
        }
    }
}